package ictgradschool.industry.lab.swing.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener
    {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton getCalculateHealthyWeight;
    private JTextField heightText;
    private JTextField weightText;
    private JTextField BMI;
    private JTextField maximumWeight;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel()
        {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        FlowLayout layout = new FlowLayout();
        this.setLayout(layout);

        JLabel label1 = new JLabel("Height in meters");
        this.add(label1);
        this.heightText = new JTextField(15);
        this.add(heightText);

        JLabel label2 = new JLabel("Weight in kilograms");
        this.add(label2);
        this.weightText = new JTextField(15);
        this.add(weightText);

        calculateBMIButton = new JButton("Calculate Button");
        this.add(calculateBMIButton);
        calculateBMIButton.addActionListener(this);

        JLabel label3 = new JLabel("Your Body Mass Index(BMI) is: ");
        this.add(label3);
        this.BMI = new JTextField(15);
        this.add(BMI);

        getCalculateHealthyWeight = new JButton("Calculate Healthy Weight");
        this.add(getCalculateHealthyWeight);
        getCalculateHealthyWeight.addActionListener(this);

        JLabel label5 = new JLabel("Maximum Healthy Weight for your Height: ");
        this.add(label5);
        this.maximumWeight = new JTextField(15);
        this.add(maximumWeight);


        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)

        // TODO Add Action Listeners for the JButtons

        }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event)
        {
        double height = 0;
        double weight = 0;
        double bmi = 0;
        double maxmimumW;
        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.
        if (event.getSource() == calculateBMIButton) {
            weight = Double.parseDouble(weightText.getText());
            height = Double.parseDouble(heightText.getText());
            bmi = roundTo2DecimalPlaces(weight / (height * height));

            BMI.setText(bmi + "");
        }
        if (event.getSource() == getCalculateHealthyWeight) {
            height = Double.parseDouble(heightText.getText());
            maxmimumW = roundTo2DecimalPlaces(24.9 * height * height);
            maximumWeight.setText(maxmimumW + "");
        }
        }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount)
        {
        return ((double) Math.round(amount * 100)) / 100;
        }

    }