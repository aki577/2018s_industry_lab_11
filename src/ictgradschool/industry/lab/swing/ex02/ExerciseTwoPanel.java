package ictgradschool.industry.lab.swing.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 * <p>
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener
    {

    /**
     * Creates a new ExerciseFivePanel.
     */
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JButton buttonAdd;
    private JButton buttonSubtract;

    public ExerciseTwoPanel()
        {
        setBackground(Color.white);
        FlowLayout layout = new FlowLayout();
        this.setLayout(layout);

        textField1 = new JTextField(10);
        this.add(textField1);
        textField2 = new JTextField(10);
        this.add(textField2);

        buttonAdd = new JButton("Add");
        this.add(buttonAdd);
        buttonAdd.addActionListener(this);

        buttonSubtract = new JButton("Subtract");
        this.add(buttonSubtract);
        buttonSubtract.addActionListener(this);

        JLabel label = new JLabel("Result: ");
        this.add(label);
        textField3 = new JTextField(20);
        this.add(textField3);
        }

    @Override
    public void actionPerformed(ActionEvent e)
        {
            double substractN;
            double addN ;
        if(e.getSource()==buttonSubtract){
            substractN = roundTo2DecimalPlaces(Double.parseDouble(textField1.getText()) - Double.parseDouble(textField2.getText()));
            textField3.setText(substractN+"");
        }
        if(e.getSource()==buttonAdd){
            addN = roundTo2DecimalPlaces(Double.parseDouble(textField1.getText()) + Double.parseDouble(textField2.getText()));
            textField3.setText(addN + "");
        }
        }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount)
        {
        return ((double) Math.round(amount * 100)) / 100;
        }

    }