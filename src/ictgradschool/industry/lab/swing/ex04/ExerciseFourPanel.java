package ictgradschool.industry.lab.swing.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import static java.awt.event.KeyEvent.*;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener
    {

    //private Balloon balloon;
//    private JButton moveButton;
    private Timer timerAnimation;
    private Timer timerCreator;
    private List<Balloon> balloons;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel()
        {
        setBackground(Color.white);
        balloons = new ArrayList<Balloon>();
        balloons.add(new Balloon(30, 60));

//        this.moveButton = new JButton("Move balloon");
//        this.moveButton.addActionListener(this);
//        this.add(moveButton);
        this.timerCreator = new Timer(1000, this);
        this.timerAnimation = new Timer(50, this);
        timerCreator.start();
        this.addKeyListener(new KeyListener()
            {
            @Override
            public void keyPressed(KeyEvent e)
                {

                for (Balloon balloon : balloons) {
                    switch (e.getKeyCode()) {
                        case VK_RIGHT:
                            timerAnimation.start();
                            balloon.setDirection(Direction.Right);
                            break;
                        case VK_LEFT:
                            timerAnimation.start();
                            balloon.setDirection(Direction.Left);
                            break;
                        case VK_UP:
                            timerAnimation.start();
                            balloon.setDirection(Direction.Up);
                            break;
                        case VK_DOWN:
                            timerAnimation.start();
                            balloon.setDirection(Direction.Down);
                            break;
                        case VK_S:
                            timerAnimation.stop();
                            break;
                    }
                }
                }

            @Override
            public void keyTyped(KeyEvent e)
                {

                }

            @Override
            public void keyReleased(KeyEvent e)
                {

                }
            });
        }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e)
        {
        int width = getWidth();
        int height = getHeight();
        if (e.getSource() == timerAnimation) {
            for (Balloon b : balloons) {
                b.move(width,height);
            }
        }
        if(e.getSource() == timerCreator){
            balloons.add(new Balloon(width/2,height/2));
        }


        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();
        repaint();
        }

    /**
     * Draws any balloons we have inside this panel.
     *
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g)
        {
        super.paintComponent(g);
        for (Balloon b : balloons) {
            b.draw(g);
        }

        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
        }

    }